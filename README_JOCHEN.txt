Jochen, July 2020

I did a few fixes in acpype, so it runs with AmberTools20 and on a modern linux:

- babel -> obabel
- babel format with -Ooutfile.mol2
- parmchk -> parmchk2
- leapAmberFile = 'leaprc.ff12SB'  -> leapAmberFile = 'leaprc.ffAM1'
